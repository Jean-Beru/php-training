/* FUNCTONS */
SELECT firstname, UPPER(lastname), DATE_FORMAT(birthdate, "%d/%m/%Y")
FROM `User`;

SELECT
  CONCAT(`firstname`, ' ', UPPER(`lastname`)) AS Name,
  FLOOR(DATEDIFF(CURRENT_DATE(), `birthdate`) / 365.25) AS Age,
  CONCAT(a.number, ', ', a.street, ', ', a.zipcode, ', ', a.city, ', ', a.country) AS Address1,
  CONCAT_WS(', ', a.number, a.street, a.zipcode, a.city, a.country) AS Address2
FROM `User` u
LEFT JOIN `UserAddress` ua ON ua.user = u.id
LEFT JOIN `Address` a ON ua.address = a.id;
