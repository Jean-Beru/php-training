/* GOLF DATABASE */
DROP DATABASE IF EXISTS golf;
CREATE DATABASE golf;
USE golf;

/* GOLF TABLES */
CREATE TABLE `user` (
    `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
    `login` varchar(20) NOT NULL,
    `password` varchar(32) NOT NULL,
    `gender` enum('M','Mme','Mlle') NOT NULL,
    `firstname` varchar(50) NOT NULL,
    `lastname` varchar(50) NOT NULL,
    `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deletedAt` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE lastPassword (
    `user` smallint(5) unsigned NOT NULL,
    `password` varchar(32) NOT NULL,
    PRIMARY KEY (`user`, `password`)
);

CREATE TABLE game (
    `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
    `course` tinyint(3) unsigned NOT NULL,
    `playedAt` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE course (
    `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
    `holeCount` tinyint(2) unsigned NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE hole (
    `id` tinyint(5) unsigned NOT NULL AUTO_INCREMENT,
    `course` tinyint(5) unsigned NOT NULL ,
    `number` tinyint(2) unsigned NOT NULL,
    `par` tinyint(2) unsigned NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE gameTry (
    `game` smallint(5) unsigned NOT NULL,
    `hole` tinyint(5) unsigned NOT NULL,
    `user` smallint(5) unsigned NOT NULL,
    `tries` tinyint(2) unsigned NOT NULL,
    PRIMARY KEY (`game`, `hole`, `user`)
);

/* GOLF FOREIGN KEYS */
ALTER TABLE `lastPassword`
ADD CONSTRAINT `IDX_lastPassword_user` FOREIGN KEY (`user`) REFERENCES `user`(`id`);

ALTER TABLE `game`
ADD CONSTRAINT `IDX_game_course` FOREIGN KEY (`course`) REFERENCES `course`(`id`);

ALTER TABLE `gameTry`
ADD CONSTRAINT `IDX_gameTry_game` FOREIGN KEY (`game`) REFERENCES `game`(`id`),
ADD CONSTRAINT `IDX_gameTry_hole` FOREIGN KEY (`hole`) REFERENCES `hole`(`id`),
ADD CONSTRAINT `IDX_gameTry_user` FOREIGN KEY (`user`) REFERENCES `user`(`id`);

ALTER TABLE `hole`
ADD CONSTRAINT `IDX_hole_course` FOREIGN KEY (`course`) REFERENCES `course`(`id`);

/* GOLF CONSTRAINTS */
ALTER TABLE `user`
ADD UNIQUE (`login`);

ALTER TABLE `hole`
ADD UNIQUE (`course`, `number`);

/* GOLF DATA */
ALTER TABLE `hole`
ADD UNIQUE (`course`, `number`);

INSERT INTO `user` (`login`, `password`, `gender`, `firstname`, `lastname`)
VALUES
    ('j.doe', MD5('JohnDoe12345'), 'M', 'John', 'Doe'),
    ('ja.doe', MD5('JaneDoe12345'), 'Mme', 'Jane', 'Doe'),
    ('jas.doe', MD5('JasonDoe12345'), 'M', 'Jason', 'Doe');

INSERT INTO `course` (`holeCount`)
VALUES (9);

SET @course = LAST_INSERT_ID();

INSERT INTO `hole` (`course`, `number`, `par`)
VALUES
    (@course, 1, 3),
    (@course, 2, 2),
    (@course, 3, 4),
    (@course, 4, 3),
    (@course, 5, 5),
    (@course, 6, 2),
    (@course, 7, 3),
    (@course, 8, 4),
    (@course, 9, 5);

INSERT INTO `course` (`holeCount`)
VALUES (9);

SET @course = LAST_INSERT_ID();

INSERT INTO `hole` (`course`, `number`, `par`)
VALUES
    (@course, 1, 3),
    (@course, 2, 2),
    (@course, 3, 4),
    (@course, 4, 3),
    (@course, 5, 5),
    (@course, 6, 2),
    (@course, 7, 3),
    (@course, 8, 4),
    (@course, 9, 5);

INSERT INTO `game` (`course`, `playedAt`)
VALUES (@course, NOW());

SET @game = LAST_INSERT_ID();

INSERT INTO `gameTry` (`game`, `hole`, `user`, `tries`)
VALUES
	(@game, 1, 1, 5),
	(@game, 1, 2, 3),
	(@game, 1, 3, 4),
	(@game, 2, 1, 3),
	(@game, 2, 2, 2),
	(@game, 2, 3, 4);

/* SELECT SCORES PER USER */
SELECT u.`firstname`, SUM(gt.`tries` - h.`par`) AS 'Score'
FROM `game` g
LEFT JOIN `gameTry` gt ON gt.`game` = g.`id`
LEFT JOIN `hole` h ON h.`id` = gt.`hole`
LEFT JOIN  `user` u on u.`id` = gt.`user`
GROUP BY gt.`user`;

/* SELECT AVERAGE TRIES PER HOLE */
SELECT h.`course`, h.`number`, h.`par`, AVG(gt.`tries`) AS 'Average', AVG(gt.`tries`) - h.`par` AS 'Diff'
FROM `hole` h
LEFT JOIN `gameTry` gt ON gt.`hole` = h.`id`
GROUP BY h.`id`;

/* DISABLE USER */
UPDATE `user`
SET `deletedAt` = NOW()
WHERE `id` = 1;

INSERT INTO `lastPassword` (`user`, `password`)
SELECT `id`, `password`
FROM `user`
WHERE `id` = 1;

UPDATE `user`
SET `pasword` = MD5('NewPassword')
WHERE `id` = 1;

/* Need `id` and `dateAdd` fields in lastPassword table
DELETE FROM `lastPassword`
WHERE `id` = (
  SELECT `id`
  FROM `lastPassword`
  WHERE `user` = 1
  ORDER BY `dateAdd` ASC
  LIMIT 1
);
*/
