/* GROUP BY */
SELECT gender, COUNT(u.id) AS total
FROM `User` u
GROUP BY u.gender;

SELECT
  u.firstname,
  u.lastname,
  COUNT(a.id) AS "Addresses count",
  GROUP_CONCAT(DISTINCT a.city SEPARATOR ', ') AS "Cities"
FROM `User` u
LEFT JOIN `UserAddress` ua ON ua.user = u .id
LEFT JOIN `Address` a ON a.id = ua.address
GROUP BY u.id;
