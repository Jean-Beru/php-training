/* HAVING */
SELECT CountryCode, name, MAX(Population) AS PopMax, Population
FROM City
GROUP BY CountryCode
HAVING PopMax = Population
ORDER BY CountryCode
