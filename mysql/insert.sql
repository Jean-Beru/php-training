/* ADD DATA */
INSERT INTO `User` (gender, firstname, lastname, birthdate)
VALUES
  ("MR", "John", "Doe", "1998-07-12"),
  ("MS", "Jane", "Doe", "2000-07-02"),
  ("MR", "Jason", "Doe", "2018-07-15"),
  ("MR", "John", "Who", "1990-01-01"),
  ("MS", "Jane", "Who", "1992-05-20");

INSERT INTO `Address` (number, street, zipcode, city, country)
VALUES
  (NULL, "Sesame street", 59000, "Lille", "FR"),
  (1, "rue de la soif", 59000, "Lille", "FR"),
  (57, "bvd de Lyon", 59000, "Lille", "FR"),
  (36, "bvd Gambetta", 75000, "Paris", "FR"),
  (1, "Oxford street", "W1C 2HW", "London", "UK");

INSERT INTO `UserAddress` (user, address)
VALUES
  (1, 1),
  (1, 3),
  (2, 4),
  (4, 2),
  (5, 2);

SELECT *
FROM City
WHERE Population >= 200000;
