SELECT ci.Name, co.Name, Population
FROM City ci
LEFT JOIN Country co ON co.Code = ci.CountryCode;

SELECT ci.Name, co.Name AS country, ci.Population
FROM `City` ci
LEFT JOIN `Country` co ON ci.CountryCode = co.Code
WHERE
  ci.Population >= 200000 AND
  co.Name LIKE "A%"
ORDER BY ci.Population ASC

SELECT c.Name, cl.Language, cl.Percentage, ci.Name
FROM `Country` c
LEFT JOIN `CountryLanguage` cl ON cl.CountryCode = c.Code
LEFT JOIN `City` ci ON c.Capital = ci.ID
WHERE cl.Percentage >= 25 ORDER BY `c`.`Name` ASC

/* IS NULL */
INSERT INTO `City` (`ID`, `Name`, `CountryCode`, `District`, `Population`)
VALUES (NULL, 'Test', 'AAA', 'Test', '100000');

SELECT c.Name, c.CountryCode
FROM `City` c
LEFT JOIN `Country` co ON co.Code = c.CountryCode
WHERE co.Code IS NULL

/* LEFT JOIN AND */
SELECT co.Name, c1.Name AS "Capital", c1.Population, c2.Name AS "Big city", c2.Population
FROM `Country` co
LEFT JOIN `City` c1 ON c1.ID = co.Capital
LEFT JOIN `City` c2 ON c2.CountryCode = co.Code AND c2.Population > 200000;

/* H/M² */
SELECT co.Name, co.Population / co.SurfaceArea AS "Densité"
FROM `Country` co
LEFT JOIN `City` c ON c.CountryCode = co.Code;

/* % pop country */
SELECT ci.Name, ci.Population, co.Population, (ci.Population * 100 / co.Population) AS "%"
FROM `City` ci
LEFT JOIN `Country` co ON ci.CountryCode = co.Code;

/* BOOL */
SELECT co.Name, c.Name AS "City", co.Capital = c.ID AS "Is capital"
FROM `Country` co
LEFT JOIN `City` c ON c.CountryCode = co.Code;

/* LEFT JOIN x 2 */
SELECT u.firstname, u.lastname, a.number, a.street, a.zipcode, a.city, a.country
FROM `User` u
LEFT JOIN `UserAddress` ua ON u.`id` = ua.`user`
LEFT JOIN `Address` a ON a.id = ua.address;
