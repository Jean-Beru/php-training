/* UPDATE */
UPDATE `User`
SET `birthdate` = "2018-01-01"
WHERE ID = 1;

UPDATE `User` u
LEFT JOIN `UserAddress` ua ON u.id = ua.user
SET `lastname` = "Toto"
WHERE ua.user IS NULL;
