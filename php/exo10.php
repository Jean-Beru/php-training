<?php

function getCardPoints(string $card): int
{
    $cards = [
        'V' => 20,
        '9' => 14,
        'A' => 11,
        '10' => 10,
        'K' => 4,
        'Q' => 3,
        '8' => 0,
        '7' => 0,
    ];

    return $cards[$card] ?? 0;
}

function getBelotePoints(array $cards): int {
    return in_array('K', $cards) && in_array('Q', $cards) ? 20 : 0;
//    return count(array_intersect($cards, ['Q', 'K'])) === 2 ? 20 : 0;
}

echo getBelotePoints(['V', 'A', 'K', 'Q']);

$cards = ['A', '9', 'Q', 'V'];
usort($cards, function(string $cardA, string $cardB): int {
    return getCardPoints($cardA) <=> getCardPoints($cardB);
});
var_dump($cards);


function getAveragePoints(array $cards): float {
    return array_sum(array_map('getCardPoints', $cards)) / count($cards);
}
var_dump(getAveragePoints(['A', '9', 'Q', 'V']));

echo implode(' | ', ['A', '9', 'Q', 'V']).PHP_EOL;


$card = 'V';
$points = 20;
echo $card.' value is '.$points.PHP_EOL;
echo "$card value is $points".PHP_EOL;
echo sprintf('%s value is %ds', $card, $points);
