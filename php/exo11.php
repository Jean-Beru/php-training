<?php

function testRegex(string $regex, array $data): void {
    foreach ($data as $value) {
        $match = preg_match($regex, $value);
        echo $value.' => '.($match ? 'OK' : 'NOK').PHP_EOL;
    }
}

echo PHP_EOL.'dd/mm/yyy'.PHP_EOL;
testRegex(
    '/^\d{2}\/\d{2}\/\d{4}$/',
    [
        '01/01/2019',
        '01/aa/2019',
        '01-01/2019',
        '001/01/2019',
    ]
);

echo PHP_EOL.'email'.PHP_EOL;
testRegex(
    '/^\w+@.+\..+$/',
    [
        'test@test.com',
        'test@test',
        'not_v@lid@test.com',
    ]
);

echo PHP_EOL.'fr number'.PHP_EOL;
testRegex(
    '/^(\+33 |0)\d([\. ]?\d{2}){4}$/',
    [
        '+33 1 23 45 67 89',
        '0123456789',
        '01 23 45 67 89',
        '01.23.45.67.89',
        '10.23.45.67.89',
    ]
);

echo PHP_EOL.'url'.PHP_EOL;
testRegex(
    '/^(https?:\/\/)?(www\.)?.+\..+(\/.+)?(\?.+)?/',
    [
        'http://google.fr',
        'https://google.fr',
        'www.google.fr',
        'www.google.fr/test?var=1',
    ]
);

echo PHP_EOL.'IP'.PHP_EOL;
testRegex(
    '/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/',
    [
        '127.0.0.1',
        '192.168.0.1',
    ]
);
