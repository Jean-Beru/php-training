<?php

class Car
{
    public $color;
    public $brand;
    public $model;
    public $hp;
    public $year;
    public $owner;

    public function __construct(string $brand, string $color)
    {
        $this->brand = $brand;
        $this->color = $color;
    }

    public function getLabel(): string
    {
        return sprintf(
            '%s %s (%d)',
            $this->brand,
            $this->model,
            $this->hp
        );
    }

    public function __toString(): string
    {
        return $this->getLabel();
    }
}

$car1 = new Car('Seat', 'Grey');
$car1->model = 'Ibiza';
$car1->hp = 75;
$car2 = new Car('Toyota', 'Black');
$car2->model = 'Auris';
$car2->hp = 90;

echo $car1->color. ' '.$car2->color.PHP_EOL;
echo $car1->getLabel(). ' '.$car2->getLabel().PHP_EOL;

echo $car1.PHP_EOL;
echo $car2.PHP_EOL;
