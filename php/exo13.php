<?php

class Address {
    private $number;
    private $street;
    private $city;

    public function __construct(int $number, string $street, string $city)
    {
        $this->number = $number;
        $this->street = $street;
        $this->city = $city;
    }

    public function getFullAddress(): string {
        return sprintf('%d %s, %s', $this->number, $this->street, $this->city);
    }

    static public function getValidCities(): array
    {
        return ['Paris', 'Lille', 'Roubaix'];
    }
}

class User {
    private $addresses = [];

    public function addAddress(Address $address) {
        $this->addresses[] = $address;
    }

    public function getAddresses(): array {
        return $this->addresses;
    }

    public function getFullAddresses(): array {
        $fullAddresses = [];
        foreach ($this->addresses as $address) {
            $fullAddresses[] = $address->getFullAddress();
        }

        return $fullAddresses;
    }
}

$address1 = new Address(10, 'rue de la Paix', 'Paris');
$address2 = new Address(14, 'rue de la Paix', 'Paris');
$address3 = new Address(16, 'rue de la Paix', 'Paris');
$user = new User();
$user->addAddress($address1);
$user->addAddress($address2);
$user->addAddress($address3);

var_dump($user->getAddresses());
var_dump($user->getFullAddresses());


var_dump($address1::getValidCities());
