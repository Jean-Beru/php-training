<?php

class Player {
    private $firstname;
    private $lastname;
    private $team;

    public function getFullname(): string {
        return $this->firstname.' '.$this->lastname;
    }

    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    public function setTeam(int $team): void
    {
        $this->team = $team;
    }

}

$player = new Player();
$player->setFirstname('John');
$player->setLastname('Doe');

echo $player->getFullname();
