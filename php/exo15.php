<?php

class Player {
    private $firstname;
    private $lastname;
    private $folds  = [];

    public function __construct(string $firstname, string $lastname)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }

    public function addFold(Fold $fold): void
    {
        $this->folds[] = $fold;
    }

    public function getPoints(string $mainColor): int
    {
        $sum = 0;
        foreach ($this->folds as $fold) {
            $sum += $fold->getPoints($mainColor);
        }

        return $sum;
    }

    public function getFullname(): string {
        return $this->firstname.' '.$this->lastname;
    }
}

class Card {
    const COLOR_PIQUE = 'pique';
    const COLOR_COEUR = 'coeur';
    const COLOR_CARREAU = 'carreau';
    const COLOR_TREFLE = 'trefle';
    const COLORS = [
        self::COLOR_PIQUE,
        self::COLOR_COEUR,
        self::COLOR_CARREAU,
        self::COLOR_TREFLE,
    ];

    const FIGURE_AS = 'as';
    const FIGURE_10 = '10';
    const FIGURE_KING = 'king';
    const FIGURE_QUEEN = 'queen';
    const FIGURE_VALET = 'valet';
    const FIGURE_9 = '9';
    const FIGURE_8 = '8';
    const FIGURE_7 = '7';
    const FIGURES = [
        self::FIGURE_AS,
        self::FIGURE_10,
        self::FIGURE_KING,
        self::FIGURE_QUEEN,
        self::FIGURE_VALET,
        self::FIGURE_9,
        self::FIGURE_8,
        self::FIGURE_7,
    ];

    private $color;
    private $figure;

    public function __construct(string $color, string $figure)
    {
        $this->color = $color;
        $this->figure = $figure;
    }

    public function getPoints(string $mainColor): int {
        if (!in_array($mainColor, self::COLORS)) {
            throw new \Exception('Invalid color');
        }

        // Couleurs égales => Atout
        if ($this->color === $mainColor) {
            $cards = [
                self::FIGURE_VALET => 20,
                self::FIGURE_9 => 14,
                self::FIGURE_AS => 11,
                self::FIGURE_10 => 10,
                self::FIGURE_KING => 4,
                self::FIGURE_QUEEN => 3,
            ];
        } else {
            $cards = [
                self::FIGURE_AS => 11,
                self::FIGURE_10 => 10,
                self::FIGURE_KING => 4,
                self::FIGURE_QUEEN => 3,
                self::FIGURE_VALET => 2,
            ];
        }

        return $cards[$this->figure] ?? 0;
    }

    public function __toString()
    {
        return $this->figure. ' de '.$this->color;
    }
}

class Fold
{
    private $cards = [];

    public function addCard(Card $card): void {
        $this->cards[] = $card;
    }

    public function getPoints(string $mainColor): int {
        $sum = 0;
        foreach ($this->cards as $card) {
            $sum += $card->getPoints($mainColor);
        }

        return $sum;
    }
}

$card1 = new Card(Card::COLOR_COEUR, Card::FIGURE_9);
$card2 = new Card(Card::COLOR_COEUR, Card::FIGURE_VALET);
$card3 = new Card(Card::COLOR_PIQUE, Card::FIGURE_VALET);
$card4 = new Card(Card::COLOR_COEUR, Card::FIGURE_QUEEN);

echo 'Card : '.$card1.' => '.$card1->getPoints(Card::COLOR_COEUR).PHP_EOL;
echo 'Card : '.$card1.' => '.$card1->getPoints(Card::COLOR_CARREAU).PHP_EOL;

$fold1 = new Fold();
$fold1->addCard($card1);
$fold1->addCard($card2);
$fold1->addCard($card3);
$fold1->addCard($card4);
$fold2 = new Fold();
$fold2->addCard($card1);
$fold2->addCard($card2);
$fold2->addCard($card3);
$fold2->addCard($card4);

echo 'Fold : '.$fold1->getPoints(Card::COLOR_COEUR).PHP_EOL;
echo 'Fold : '.$fold1->getPoints(Card::COLOR_PIQUE).PHP_EOL;

$player = new Player('John', 'Doe');
$player->addFold($fold1);
$player->addFold($fold2);

echo 'Player : '.$player->getPoints(Card::COLOR_COEUR);
