<?php

require_once(__DIR__.'/init_db.php');
require_once(__DIR__.'/product.php');

try {
    // Drop and create table
    $pdo->exec('DROP TABLE IF EXISTS product;');
    $pdo->exec('CREATE TABLE product (`name` VARCHAR(40), `price` SMALLINT UNSIGNED);');

    // Insert 10 products
    for ($i = 0; $i < 10; $i++) {
        $price = \rand(100, 3000);
        $pdo->exec("INSERT INTO product (`name`, `price`) VALUES ('Product #$i', $price);");
    }

    // Insert 10 products
    for ($i = 10; $i < 20; $i++) {
        $price = \rand(100, 3000);
        $stmt = $pdo->prepare('INSERT INTO product (`name`, `price`) VALUES (:name, :price);');
        $stmt->bindValue(':name', 'Product #'.$i, \PDO::PARAM_STR);
        $stmt->bindValue(':price', $price, \PDO::PARAM_INT);
        $stmt->execute();
    }

    // Get data
    $stmt = $pdo->query('SELECT `name`, `price` FROM `product`;');
    echo PHP_EOL.'Data as array'.PHP_EOL;
    while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
        echo $row['name'].' '.$row['price'].'€ '.(\floor($row['price']*1.196)).'€'.PHP_EOL;
    }

    // Get data as object
    $stmt = $pdo->query('SELECT `name`, `price` FROM `product`;');
    $stmt->setFetchMode(PDO::FETCH_CLASS, Product::class);
    echo PHP_EOL.'Data as object'.PHP_EOL;
    while ($product = $stmt->fetch()) {
        echo $product->getName().' '.$product->getPrice().'€ '.$product->getVatPrice().'€'.PHP_EOL;
    }
} catch (\PDOException $e) {
    echo 'Erreur: '.$e->getMessage();
    exit;
}
