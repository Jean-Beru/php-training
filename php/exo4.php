<?php

$card = 'A';

echo "$card => ";

if ($card === 'V') {
    echo 20;
} elseif ($card === '9') {
    echo 14;
} elseif ($card === 'A') {
    echo 11;
} elseif ($card === '10') {
    echo 10;
} elseif ($card === 'K') {
    echo 4;
} elseif ($card === 'Q') {
    echo 3;
} else {
    echo 0;
}
