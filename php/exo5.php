<?php

$cards = [
    'V' => 20,
    '9' => 14,
    'A' => 11,
    '10' => 10,
    'K' => 4,
    'Q' => 3,
    '8' => 0,
    '7' => 0,
];

foreach ($cards as $card => $points) {
    $plural = $points > 1 ? 's' : '';
    echo "Card $card = $points point$plural<br />";
}

$sum = 0;
$fold = ['V', '9', '10', 'Q'];
foreach ($fold as $card) {
    $sum += $cards[$card];
}
$plural = $sum > 1 ? 's' : '';
echo "Fold has $sum point$plural";
