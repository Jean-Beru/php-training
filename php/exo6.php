<?php

$card = '9';

switch($card) {
    case 'V':
        $value = 20;
        break;
    case '9':
        $value = 14;
        break;
    case 'A':
        $value = 11;
        break;
    case '10':
        $value = 10;
        break;
    case 'K':
        $value = 4;
        break;
    case 'Q':
        $value = 3;
        break;
    default:
        $value = 0;
}

echo "$card = $value";
echo PHP_EOL;
