<?php

/**
 * @param string $card 'V', '9', 'A', etc...
 * @return int
 */
function getCardPoints($card)
{
//    switch($card) {
//        case 'V':
//            return 20;
//        case '9':
//            return 14;
//        case 'A':
//            return 11;
//        case '10':
//            return 10;
//        case 'K':
//            return 4;
//        case 'Q':
//            return 3;
//        default:
//            return 0;
//    }
    $cards = [
        'V' => 20,
        '9' => 14,
        'A' => 11,
        '10' => 10,
        'K' => 4,
        'Q' => 3,
        '8' => 0,
        '7' => 0,
    ];

    return $cards[$card] ?? 0;
}

function getFoldPoints($cards) {
    $sum = 0;
    foreach ($cards as $card) {
        $sum += getCardPoints($card);
    }

    return $sum;
}

echo getCardPoints('A').PHP_EOL;
echo getFoldPoints(['V', '9', 'A', 'Q']).PHP_EOL;
