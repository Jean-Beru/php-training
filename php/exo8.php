<?php

$cards = [];
for ($i = 1; $i <= 32; $i++) {
    $cards[] = "Carte n°$i";
}

function getPlayerCards($cards, $player = 1) {
    $playerCards = [];
    $start = ($player - 1) * 5;
    for ($i = 1; $i <= 5; $i++) {
        $playerCards[] = $cards[$start + $i];
    }

    return $playerCards;
//    return array_slice($cards, $start, 5);
}

var_dump(getPlayerCards($cards));
var_dump(getPlayerCards($cards, 1)); // Same as previous
var_dump(getPlayerCards($cards, 2));
