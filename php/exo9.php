<?php

/**
 * @param string $card 'V', '9', 'A', etc...
 * @return int
 */
function getCardPoints($card)
{
    $cards = [
        'V' => 20,
        '9' => 14,
        'A' => 11,
        '10' => 10,
        'K' => 4,
        'Q' => 3,
        '8' => 0,
        '7' => 0,
    ];

    return $cards[$card] ?? 0;
}

function getFoldPoints(...$cards) {
//    echo 'Nb cards : ' . \count($cards).PHP_EOL;
    $sum = 0;
    foreach ($cards as $card) {
        $sum += getCardPoints($card);
    }

    return $sum;
}

echo getFoldPoints('V', '9', 'A', 'Q').PHP_EOL;
