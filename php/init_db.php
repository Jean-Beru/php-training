<?php

$host = 'database';
$db = 'amigraf';
$user = 'user';
$pass = 'pwd';

$dsn = "mysql:host=$host;dbname=$db;";

try {
    $pdo = new \PDO($dsn, $user, $pass);
    $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
} catch (\PDOException $e) {
    echo 'Erreur: '.$e->getMessage();
    exit;
}
