<?php

class Product
{
    private $name;
    private $price;

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getVatPrice(): int
    {
        return \floor($this->price * 1.196);
    }
}
