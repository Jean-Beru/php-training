<?php

require_once('../src/debug.php');
require_once('../src/init_db.php');
require_once('../src/comment.php');

try {
    // Add comment
    if (isset($_POST['title']) && isset($_POST['comment']) && isset($_POST['news_id'])) {
        $stmt = $pdo->prepare('
            INSERT INTO `comment` (`title`, `comment`, `news`)
            VALUES (:title, :comment, :news);
        ');
        $stmt->bindValue(':title', $_POST['title'], \PDO::PARAM_STR);
        $stmt->bindValue(':comment', $_POST['comment'], \PDO::PARAM_STR);
        $stmt->bindValue(':news', $_POST['news_id'], \PDO::PARAM_INT);
        $stmt->execute();
    }

    // List comments
    $stmt = $pdo->query('
        SELECT `title`, `comment`
        FROM `comment`
        ORDER BY `id` DESC
        LIMIT 5;
    ');
    $stmt->setFetchMode(\PDO::FETCH_CLASS, Comment::class);

} catch(\PDOException $e) {
    echo $e->getMessage();
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form example</title>
</head>
<body>
    <div>
        <h1>5 derniers commentaires</h1>
        <ul>
            <?php while($row = $stmt->fetch()) : ?>
                <li>
                    Title : <?php echo $row->getTitle(); ?><br />
                    Comment :  <?php echo $row->getComment(); ?><br />
                    Test :  <?php echo $row->getTest(); ?>
                </li>
            <?php endwhile; ?>
        </ul>
    </div>Ajouter un commentaire</h1>
    <form action="" method="POST">
        <label>Title</label>
        <input type="text" name="title" />

        <br />

        <label>Comment</label>
        <textarea name="comment" rows="5"></textarea>

        <br />

        <input type="hidden" name="news_id" value="42" />

        <input type="submit" value="Commenter" />
    </form>
</body>
</html>
