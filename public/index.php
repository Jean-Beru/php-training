<?php
    $exercice = filter_var($_GET['exercise'], FILTER_SANITIZE_NUMBER_INT);
?>
<html>
<head>
    <title>Amigraf</title>
    <style>
        #menu li { display: inline-block; }
        #code, #result {
            background-color: #333;
            color: #eee;
            display: block;
            line-height: .7em;
            margin: 1em;
            padding: 1em;
            white-space: pre;
        }
        #result {
            line-height: initial;
        }
    </style>
</head>
<body>
    <?php include('./menu.php'); ?>

    <?php if (!$exercice) : ?>
        <h2>Welcome !</h2>
    <?php else : ?>
        <h2>Exo #<?php echo $exercice; ?></h2>

        <h3>Result</h3>
        <div id="result">
            <?php
                $file = "../php/exo$exercice.php";

                include($file);
            ?>
        </div>

        <h3>Answer</h3>
        <pre id="code"><?php echo nl2br(htmlentities(file_get_contents($file))); ?></pre>
    <?php endif ?>
</body>
</html>
