<?php
  $exerciceCount = \count(\glob('../php/exo*.php'));
?>
<ul id="menu">
    <li><a href="/">Homepage</a></li>
    <?php for ($i = 1; $i <= $exerciceCount; $i++) : ?>
        <li><a href="?exercise=<?php echo $i; ?>">Exo <?php echo $i; ?></a></li>
    <?php endfor ?>
</ul>
