<?php

class Comment
{
    private $title;
    private $comment;
    private $news;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function getTest(): string {
        return '#'.$this->news.' '.$this->title;
    }
}
