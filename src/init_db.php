<?php

$host = 'database';
$db = 'amigraf';
$user = 'user';
$pass = 'pwd';

$dsn = "mysql:host=$host;dbname=$db;";

try {
    $pdo = new \PDO($dsn, $user, $pass);
    $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    // Create table "comment"
    // title, comment, news
    $pdo->exec('CREATE TABLE IF NOT EXISTS `comment` (
        `id` INT UNSIGNED AUTO_INCREMENT,
        `title` VARCHAR(50),
        `comment` TEXT,
        `news` SMALLINT UNSIGNED,
        PRIMARY KEY(`id`)
    );');

} catch (\PDOException $e) {
    echo 'Erreur: '.$e->getMessage();
    exit;
}
